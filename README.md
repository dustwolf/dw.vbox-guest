# Ansible vbox guest

Ansible for installing virtualbox guest additions.

Currently made to work with CentOS 8 stream Linux.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.
